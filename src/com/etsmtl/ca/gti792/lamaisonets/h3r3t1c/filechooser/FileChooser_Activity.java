package com.etsmtl.ca.gti792.lamaisonets.h3r3t1c.filechooser;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.etsmtl.ca.gti792.lamaisonets.activity.R;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Cette classe permet de d�marrer une activit� de listage permettant de s�lectionner un fichier � l'int�rieur de l'arborescence de
 * la carte SD de l'appareil qui d�marre l'activit�. Dans le cadre de cette application, elle permet entre autre de r�cup�rer
 * l'adressage de fichiers LIRC. Par d�faut, le r�pertoire contenant les fichiers est d�finie par:
 * <code>Environment.getExternalStorageDirectory().getPath()+"/tmp"</code><BR />
 * <BR />
 * 
 * Inspiration: Creating Simple File Chooser - Android Development | Dream.In.Code. 2013. Creating Simple File Chooser - Android
 * Development | Dream.In.Code. [ONLINE] Available at: http://www.dreamincode.net/forums/topic/190013-creating-simple-file-chooser/.
 * [Accessed 11 July 2013].
 * 
 * @author Fran�ois
 * 
 */
public class FileChooser_Activity extends ListActivity {

	private File currentDir;
	private FileChooserFileArrayAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		currentDir = new File(Environment.getExternalStorageDirectory().getPath() + "/tmp");
		fill(currentDir);
	}

	private void fill(File f) {
		File[] dirs = f.listFiles();
		this.setTitle("Current Dir: " + f.getName());
		List<FileChooserOptionHelper> dir = new ArrayList<FileChooserOptionHelper>();
		List<FileChooserOptionHelper> fls = new ArrayList<FileChooserOptionHelper>();
		try {
			for (File ff : dirs) {
				if (ff.isDirectory())
					dir.add(new FileChooserOptionHelper(ff.getName(), "Folder", ff.getAbsolutePath()));
				else {
					fls.add(new FileChooserOptionHelper(ff.getName(), "File Size: " + ff.length(), ff.getAbsolutePath()));
				}
			}
		} catch (Exception e) {

		}
		Collections.sort(dir);
		Collections.sort(fls);
		dir.addAll(fls);
		if (!f.getName().equalsIgnoreCase("sdcard"))
			dir.add(0, new FileChooserOptionHelper("..", "Parent Directory", f.getParent()));
		adapter = new FileChooserFileArrayAdapter(FileChooser_Activity.this, R.layout.filechooser_file_view, dir);
		this.setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		FileChooserOptionHelper o = adapter.getItem(position);
		if (o.getData().equalsIgnoreCase("folder") || o.getData().equalsIgnoreCase("parent directory")) {
			currentDir = new File(o.getPath());
			fill(currentDir);
		} else {
			onFileClick(o);
		}
	}

	private void onFileClick(FileChooserOptionHelper o) {
		Toast.makeText(this, "Fichier s�lectionn�: " + o.getName(), Toast.LENGTH_SHORT).show();
		Intent resultData = new Intent();

		resultData.putExtra("file", o.getPath());
		setResult(Activity.RESULT_OK, resultData);
		finish();
	}
}