package com.etsmtl.ca.gti792.lamaisonets.h3r3t1c.filechooser;

import java.util.List;

import com.etsmtl.ca.gti792.lamaisonets.activity.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Cette classe permet de d�marrer une activit� de listage permettant de s�lectionner un fichier � l'int�rieur de l'arborescence de
 * la carte SD de l'appareil qui d�marre l'activit�. Dans le cadre de cette application, elle permet entre autre de r�cup�rer
 * l'adressage de fichiers LIRC. Par d�faut, le r�pertoire contenant les fichiers est d�finie par:
 * <code>Environment.getExternalStorageDirectory().getPath()+"/tmp"</code><BR />
 * <BR />
 * 
 * Inspiration: Creating Simple File Chooser - Android Development | Dream.In.Code. 2013. Creating Simple File Chooser - Android
 * Development | Dream.In.Code. [ONLINE] Available at: http://www.dreamincode.net/forums/topic/190013-creating-simple-file-chooser/.
 * [Accessed 11 July 2013].
 * 
 * @author Fran�ois
 * 
 */
public class FileChooserFileArrayAdapter extends ArrayAdapter<FileChooserOptionHelper> {

	private Context c;
	private int id;
	private List<FileChooserOptionHelper> items;

	public FileChooserFileArrayAdapter(Context context, int textViewResourceId, List<FileChooserOptionHelper> objects) {
		super(context, textViewResourceId, objects);
		c = context;
		id = textViewResourceId;
		items = objects;
	}

	public FileChooserOptionHelper getItem(int i) {
		return items.get(i);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(id, null);
		}
		final FileChooserOptionHelper o = items.get(position);
		if (o != null) {
			TextView t1 = (TextView) v.findViewById(R.id.TextView01);
			TextView t2 = (TextView) v.findViewById(R.id.TextView02);

			if (t1 != null)
				t1.setText(o.getName());
			if (t2 != null)
				t2.setText(o.getData());

		}
		return v;
	}

}
