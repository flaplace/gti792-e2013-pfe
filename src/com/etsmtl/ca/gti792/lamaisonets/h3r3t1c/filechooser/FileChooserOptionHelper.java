package com.etsmtl.ca.gti792.lamaisonets.h3r3t1c.filechooser;

import java.util.Locale;

/**
 * Cette classe permet de d�marrer une activit� de listage permettant de s�lectionner un fichier � l'int�rieur de l'arborescence de
 * la carte SD de l'appareil qui d�marre l'activit�. Dans le cadre de cette application, elle permet entre autre de r�cup�rer
 * l'adressage de fichiers LIRC. Par d�faut, le r�pertoire contenant les fichiers est d�finie par:
 * <code>Environment.getExternalStorageDirectory().getPath()+"/tmp"</code><BR />
 * <BR />
 * 
 * Inspiration: Creating Simple File Chooser - Android Development | Dream.In.Code. 2013. Creating Simple File Chooser - Android
 * Development | Dream.In.Code. [ONLINE] Available at: http://www.dreamincode.net/forums/topic/190013-creating-simple-file-chooser/.
 * [Accessed 11 July 2013].
 * 
 * @author Fran�ois
 * 
 */
public class FileChooserOptionHelper implements Comparable<FileChooserOptionHelper> {
	private String name;
	private String data;
	private String path;

	public FileChooserOptionHelper(String n, String d, String p) {
		name = n;
		data = d;
		path = p;
	}

	public String getName() {
		return name;
	}

	public String getData() {
		return data;
	}

	public String getPath() {
		return path;
	}

	@Override
	public int compareTo(FileChooserOptionHelper o) {
		if (this.name != null)
			return this.name.toLowerCase(Locale.CANADA_FRENCH).compareTo(o.getName().toLowerCase());
		else
			throw new IllegalArgumentException();
	}
}
