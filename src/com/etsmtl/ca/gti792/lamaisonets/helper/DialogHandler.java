package com.etsmtl.ca.gti792.lamaisonets.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Class permettant de cr�er un DialogAlert capable de communiquer l'action choisie par l'utilisateur.
 * 
 * Inspiration: http://stackoverflow.com/questions/10763986/android-confirmation-dialog-returning-true-or-false
 * @author Fran�ois
 *
 */
public class DialogHandler {
    public Runnable ans_true = null;
    public Runnable ans_false = null;

    public boolean Confirm(Activity act, String Title, String ConfirmText,
            String CancelBtn, String OkBtn, Runnable aProcedure, Runnable bProcedure) {
        ans_true = aProcedure;
        ans_false= bProcedure;
        AlertDialog dialog = new AlertDialog.Builder(act).create();
        dialog.setTitle(Title);
        dialog.setMessage(ConfirmText);
        dialog.setCancelable(true);
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, OkBtn,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {
                         ans_true.run();
                    }
                });
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, CancelBtn,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int buttonId) {
                        ans_false.run();
                    }
                });
        dialog.setIcon(android.R.drawable.ic_dialog_alert);
        dialog.show();
        return true;
    }
}