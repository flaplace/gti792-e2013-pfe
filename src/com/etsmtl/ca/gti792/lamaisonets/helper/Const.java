package com.etsmtl.ca.gti792.lamaisonets.helper;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Classe de constantes
 * 
 * @author Fran�ois
 *
 */
public class Const {

	public static ArrayList<String> getActivityOptions() {
		final ArrayList<String> activity_options = new ArrayList<String>(Arrays.asList("Ouvrir activit�", "Programmer activit�",
				"Supprimer activit�"));
		return activity_options;
	}

	public static ArrayList<String> getDefaultActivitiesTypes() {
		final ArrayList<String> default_activities = new ArrayList<String>(Arrays.asList("Controle lumiere", "Controle televiseur",
				"Controle robot", "Controle autre"));
		return default_activities;
	}

	public static ArrayList<String> getClockFormat() {
		return new ArrayList<String>(Arrays.asList("12h","24h"));
	}

	public static ArrayList<String> getLanguageList() {
		return new ArrayList<String>(Arrays.asList("Fran�ais", "English"));
	}
	
	
}
