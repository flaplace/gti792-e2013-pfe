package com.etsmtl.ca.gti792.lamaisonets.helper;

import java.io.File;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

/**
 * Classe permettant toutes les int�ractions avec la base de donn�es embarqu�e SQLite de l'application SQLite. Cette classe manipule
 * directment les objets dans les tables, mais devrait � l'avenir utiliser les classes objets d�finies dans le paquetage
 * com.etsmtl.ca.gti792.dbmodel.<BR />
 * <BR />
 * 
 * Inspiration: Android� For Programmers, An App-Driven Approach Deitel� Developer Series
 * 
 * @author Fran�ois
 * 
 */
public class DatabaseConnectorHelper {
	private static DatabaseConnectorHelper instance = null;
	private static DatabaseOpenHelper databaseOpenHelper = null; // database helper
	// database name
	// cd /data/data/com.etsmtl.ca.gti792.lamaisonets.activity/databases
	private static final String DATABASE_NAME = "lamaisonets";
	private SQLiteDatabase database; // database object
	private String profil_name;
	private int profilId = -1;

	public String getProfil_name() {
		return profil_name;
	}

	public static DatabaseConnectorHelper getInstance(Context context) {
		if (instance == null)
			instance = new DatabaseConnectorHelper();
		if (databaseOpenHelper == null) {
			databaseOpenHelper = instance.new DatabaseOpenHelper(context, DATABASE_NAME, null, 1);
			instance.loadProfils();
		}
		return instance;
	}

	private void loadProfils() {
		ArrayList<String> mArrayList = getProfil_list();
		if (mArrayList.size() == 0) {
			insertNewProfile("Profil_1");
			this.profil_name = "Profil_1";
			this.profilId = getProfilId("Profil_1");
			mArrayList = getProfil_list();
		} else {
			this.profil_name = mArrayList.get(0);
			this.profilId = getProfilId(getProfil_name());
		}
	}

	private DatabaseConnectorHelper() {

	}

	public void deleteActivityFromProfil(String activityName) {
		int activityId = -1;
		activityId = getActivityIdFromActivityName(activityName);
		if (profilId != -1 && activityId != -1) {
			open();
			String query = "DELETE FROM profil_activite WHERE idProfil='" + profilId + "' AND idActivite='" + activityId + "'";
			database.execSQL(query);
			close();
			open();
			query = "DELETE FROM activite WHERE nomActivite='" + activityName + "'";
			database.execSQL(query);
			close();
		}

	}

	public void deleteProfil() {
		open();
		String query = "DELETE FROM profil where idProfil='" + profilId + "'";
		database.execSQL(query);
		close();
	}

	public ArrayList<String> getActivity_names() {
		ArrayList<Integer> activityIds = getActivityIdsFromIdProfil();

		open();
		Cursor mCursor = database.query("Activite", null, null, null, null, null, null);
		int nomActiviteIndex = mCursor.getColumnIndex("nomActivite");
		int idActivityIndex = mCursor.getColumnIndex("idActivite");
		ArrayList<String> mArrayList = new ArrayList<String>();
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			for (Integer idActivity : activityIds) {
				if (idActivity == mCursor.getInt(idActivityIndex))
					mArrayList.add(mCursor.getString(nomActiviteIndex));
			}
		}
		close();
		return mArrayList;
	}

	public ArrayList<Integer> getActivityIdsFromIdProfil() {
		profilId = getProfilId(getProfil_name());
		open();
		Cursor mCursor = database.query("profil_activite", null, "idProfil='" + profilId + "'", null, null, null, null);
		int idActivityIndex = mCursor.getColumnIndex("idActivite");
		ArrayList<Integer> mArrayList = new ArrayList<Integer>();
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			mArrayList.add(mCursor.getInt(idActivityIndex));
		}
		close();
		return mArrayList;
	}

	public int getActivityTypeIdFromActivityTypeName(String nomTypeActivite) {
		open();
		Cursor mCursor = database.query("TypeActivite", null, null, null, null, null, null);
		int idTypeActiviteIndex = mCursor.getColumnIndex("idTypeActivite");
		int nomActiviteIndex = mCursor.getColumnIndex("nomActivite");
		int idTypeActivite = 0;
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (mCursor.getString(nomActiviteIndex).equalsIgnoreCase(nomTypeActivite)) {
				idTypeActivite = mCursor.getInt(idTypeActiviteIndex);
				break;
			}
		}
		close();
		return idTypeActivite;
	}

	public String getActiviteTypeNameFromActivityName(String activityName) {
		int activityTypeId = getActivityTypeIdFromActivityName(activityName);

		open();
		Cursor mCursor = database.query("TypeActivite", null, null, null, null, null, null);
		int nomActiviteIndex = mCursor.getColumnIndex("nomActivite");
		int idTypeActiviteIndex = mCursor.getColumnIndex("idTypeActivite");
		String nomActivite = null;
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (mCursor.getInt(idTypeActiviteIndex) == (activityTypeId)) {
				nomActivite = mCursor.getString(nomActiviteIndex);
				break;
			}
		}
		close();
		return nomActivite;
	}

	public int getActivityIdFromActivityName(String nomActivite) {
		open();
		Cursor mCursor = database.query("activite", null, null, null, null, null, null);
		int nomActiviteIndex = mCursor.getColumnIndex("nomActivite");
		int idActiviteIndex = mCursor.getColumnIndex("idActivite");

		int idActivite = -1;
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (nomActivite.equals((mCursor.getString(nomActiviteIndex)))) {
				idActivite = mCursor.getInt(idActiviteIndex);
				break;
			}
		}
		close();
		return idActivite;
	}

	public int getActivityTypeIdFromActivityName(String nomActivite) {
		open();
		Cursor mCursor = database.query("activite", null, null, null, null, null, null);
		int nomActiviteIndex = mCursor.getColumnIndex("nomActivite");
		int idTypeActiviteIndex = mCursor.getColumnIndex("idTypeActivite");

		int idTypeActivite = -1;
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (nomActivite.equals((mCursor.getString(nomActiviteIndex)))) {
				idTypeActivite = mCursor.getInt(idTypeActiviteIndex);
				break;
			}
		}
		close();
		return idTypeActivite;
	}

	public ArrayList<String> getActivity_options() {
		return Const.getActivityOptions();
	}

	public ArrayList<String> getActivity_types() {
		open();
		Cursor mCursor = database.query("TypeActivite", null, null, null, null, null, null);
		int nomTypeActiviteIndex = mCursor.getColumnIndex("nomActivite");
		ArrayList<String> mArrayList = new ArrayList<String>();
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			mArrayList.add(mCursor.getString(nomTypeActiviteIndex));
		}
		close();
		return mArrayList;
	}

	public String getFileNameFromActivityName(String nomActivite) {
		// Get idTypeActivity
		open();
		Cursor mCursor = database.query("activite", null, null, null, null, null, null);
		int nomActiviteIndex = mCursor.getColumnIndex("nomActivite");
		int emplacementFichierIndex = mCursor.getColumnIndex("emplacementFichier");
		String emplacementFichier = null;
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (nomActivite.equals(mCursor.getString(nomActiviteIndex))) {
				emplacementFichier = mCursor.getString(emplacementFichierIndex);
				break;
			}
		}
		close();
		return emplacementFichier;
	}

	public ArrayList<String> getLirc_list() {
		open();
		Cursor mCursor = database.query("fichierlirc", null, null, null, null, null, null);
		int emplacementFichierIndex = mCursor.getColumnIndex("emplacementFichier");
		ArrayList<String> mArrayList = new ArrayList<String>();
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			mArrayList.add(mCursor.getString(emplacementFichierIndex));
		}
		close();
		return mArrayList;
	}

	public ArrayList<String> getLirc_listTrimmed() {
		ArrayList<String> mArrayList = getLirc_list();
		ArrayList<String> mArrayList2 = new ArrayList<String>();
		for (String lirc : mArrayList) {
			int lastIndex = lirc.lastIndexOf("/");
			if (lastIndex != -1)
				mArrayList2.add(lirc.substring(lastIndex));
		}
		return mArrayList2;

	}

	public int getLIRCIdFromPath(String path) {
		open();
		Cursor mCursor = database.query("fichierlirc", null, null, null, null, null, null);
		int idFichierLIRCIndex = mCursor.getColumnIndex("idFichierLIRC");
		int emplacementFichierIndex = mCursor.getColumnIndex("emplacementFichier");
		int idFichierLIRC = -1;
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (mCursor.getString(emplacementFichierIndex).equalsIgnoreCase(path)) {
				idFichierLIRC = mCursor.getInt(idFichierLIRCIndex);
				break;
			}
		}
		close();
		return idFichierLIRC;
	}

	public ArrayList<String> getProfil_list() {
		open();
		Cursor mCursor = database.query("profil", null, null, null, null, null, null);
		int nomProfilIndex = mCursor.getColumnIndex("nomProfil");
		ArrayList<String> mArrayList = new ArrayList<String>();
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			mArrayList.add(mCursor.getString(nomProfilIndex));
		}
		close();

		return mArrayList;
	}

	/**
	 * Method to retrieve the profil id from sqlite database, if profil name exists
	 * 
	 * @param nomProfil
	 * @return profil id if found, otherwise -1
	 */
	public int getProfilId(String nomProfil) {
		open();
		Cursor mCursor = database.query("profil", null, "nomProfil='" + nomProfil + "'", null, null, null, null);
		int idProfilIndex = mCursor.getColumnIndex("idProfil");
		mCursor.moveToFirst();
		try {
			profilId = mCursor.getInt(idProfilIndex);
		} catch (Exception e) {
		}
		close();

		return profilId;
	}

	public void insertNewActivity(String nomActivite, String pathFichier, String profil) {
		// verify activity does not exist already..
		selectProfil(profil);
		open();
		Cursor mCursor = database.query("activite", new String[] { "nomActivite" }, null, null, null, null, null);
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (mCursor.getString(0).equalsIgnoreCase(nomActivite)) {
				open();
				database.delete("activite", "nomActivite= '" + nomActivite + "'", null);
				close();
				break;
			}
		}
		close();

		int idFichierLIRC = getLIRCIdFromPath(pathFichier);
		int idTypeActivite = getActivityTypeIdFromIdFichierLIRC(idFichierLIRC);

		// insert into activite
		open();
		String query = "INSERT INTO activite (nomActivite, idTypeActivite, emplacementFichier) VALUES ( '" + nomActivite + "', '"
				+ idTypeActivite + "', '" + pathFichier + "')";
		database.execSQL(query);
		close();

		// get idActivite
		int idActivite = -1;
		idActivite = getActivityIdFromActivityName(nomActivite);

		// insert into profil_activite
		if (idActivite != -1) {
			open();
			query = "INSERT INTO profil_activite (idProfil, idActivite) VALUES ( '" + profilId + "', '" + idActivite + "')";
			database.execSQL(query);
			close();
		}
	}

	public int getActivityTypeIdFromIdFichierLIRC(int idFichierLIRC) {
		open();
		Cursor mCursor = database.query("activite_fichierlirc", null, null, null, null, null, null);
		int idFichierLIRCIndex = mCursor.getColumnIndex("idFichierLIRC");
		int idTypeActiviteIndex = mCursor.getColumnIndex("idTypeActivite");
		int idTypeActivite = 0;
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (idFichierLIRC == (mCursor.getInt(idFichierLIRCIndex))) {
				idTypeActivite = mCursor.getInt(idTypeActiviteIndex);
				break;
			}
		}
		close();
		return idTypeActivite;
	}

	public void insertNewProfile(String name) {

		// verify config file does not exist already..
		boolean add = true;

		ArrayList<String> profilList = getProfil_list();
		for (String profil : profilList) {
			if (name.equals(profil)) {
				add = false;
				break;
			}
		}

		// add new profile
		if (add) {
			ContentValues newProfil = new ContentValues();
			newProfil.put("nomProfil", name);
			open();
			database.insert("profil", null, newProfil);
			selectProfil(name);
			close();
		}
	}

	public void insertNewLIRCFile(String path, String nomTypeActivite) {

		// verify config file does not exist already..
		boolean add = true;
		open();
		Cursor mCursor = database.query("fichierlirc", new String[] { "emplacementFichier" }, null, null, null, null, null);
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			if (mCursor.getString(0).equalsIgnoreCase(path)) {
				add = false;
				int idFichierLIRC = getLIRCIdFromPath(path);
				open();
				database.delete("Activite_FichierLIRC", "idFichierLIRC=" + idFichierLIRC, null);
				close();
				break;
			}
		}
		close();

		// add fichier lirc
		ContentValues newLircFile = new ContentValues();
		newLircFile.put("emplacementFichier", path);
		open();
		if (add)
			database.insert("fichierlirc", null, newLircFile);
		close();

		// add Activite_FichierLIRC
		// get idFichierLIRC
		int idFichierLIRC = getLIRCIdFromPath(path);

		// get idTypeActivite
		int idTypeActivite = getActivityTypeIdFromActivityTypeName(nomTypeActivite);

		open();
		ContentValues newActivite_fichierlirc = new ContentValues();
		newActivite_fichierlirc.put("idFichierLIRC", idFichierLIRC);
		newActivite_fichierlirc.put("idTypeActivite", idTypeActivite);
		database.insert("Activite_FichierLIRC", null, newActivite_fichierlirc);
		close();
	}

	public ArrayList<String> selectActivityFromProfil(String nomProfil) {
		int id = getProfilId(nomProfil);
		ArrayList<String> activityNames;
		if (id != -1) {
			selectProfil(nomProfil);
			profilId = id;
			activityNames = getActivity_names();
		} else {
			loadProfils();
			activityNames = selectActivityFromProfil(profil_name);
		}
		return activityNames;
	}

	public void selectProfil(String string) {
		ArrayList<String> profil_list = getProfil_list();
		for (String profil : profil_list) {
			if (profil.equals(string)) {
				profil_name = string;
				profilId = getProfilId(profil_name);
			}
		}
	}

	// open the database connection
	public void open() throws SQLException {
		// create or open a database for reading/writing
		database = databaseOpenHelper.getWritableDatabase();
	} // end method open

	// close the database connection
	public void close() {
		if (database != null)
			database.close(); // close the database connection
	} // end method close

	public void flushDB() {
		File mDatabaseFile = databaseOpenHelper.getmDatabaseFile();
		if (mDatabaseFile != null)
			mDatabaseFile.delete();
	}

	private class DatabaseOpenHelper extends SQLiteOpenHelper {
		// public constructor
		private final File mDatabaseFile;

		public File getmDatabaseFile() {
			return mDatabaseFile;
		}

		public DatabaseOpenHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
			mDatabaseFile = context.getDatabasePath(name);
		} // end DatabaseOpenHelper constructor

		@Override
		public synchronized SQLiteDatabase getWritableDatabase() {
			return super.getWritableDatabase();
		}

		// creates the contacts table when the database is created
		@Override
		public void onCreate(SQLiteDatabase db) {

			// Creating tables
			String query = "CREATE  TABLE IF NOT EXISTS Profil (idProfil INTEGER PRIMARY KEY AUTOINCREMENT,nomProfil VARCHAR(45) NOT NULL)";
			db.execSQL(query);
			query = "CREATE  TABLE IF NOT EXISTS FichierLIRC (  idFichierLIRC INTEGER PRIMARY KEY AUTOINCREMENT,  emplacementFichier VARCHAR(90) NOT NULL)";
			db.execSQL(query);
			query = "CREATE  TABLE IF NOT EXISTS Activite (  idActivite  INTEGER PRIMARY KEY AUTOINCREMENT,  nomActivite VARCHAR(45) NOT NULL ,  idTypeActivite INT NOT NULL , emplacementFichier VARCHAR(90) NOT NULL,  CONSTRAINT FK3    FOREIGN KEY (idTypeActivite )    REFERENCES Activite_FichierLIRC (idTypeActivite )    ON DELETE CASCADE    ON UPDATE CASCADE)";
			db.execSQL(query);
			query = "CREATE  TABLE IF NOT EXISTS Profil_Activite (  idProfil INT NOT NULL ,  idActivite INT NOT NULL ,  PRIMARY KEY (idProfil, idActivite) ,  CONSTRAINT fk_Profil_Activite_Profil1    FOREIGN KEY (idProfil )    REFERENCES Profil (idProfil )    ON DELETE CASCADE    ON UPDATE CASCADE,  CONSTRAINT fk_Profil_Activite_Activite1    FOREIGN KEY (idActivite )    REFERENCES Activite (idActivite )    ON DELETE CASCADE    ON UPDATE CASCADE)";
			db.execSQL(query);
			query = "CREATE  TABLE IF NOT EXISTS Preference (  idProfil  INTEGER PRIMARY KEY AUTOINCREMENT ,  langue VARCHAR(2) NOT NULL ,  formatHeure INT NOT NULL ,  CONSTRAINT FK5    FOREIGN KEY (idProfil )    REFERENCES Profil (idProfil )    ON DELETE CASCADE    ON UPDATE CASCADE)";
			db.execSQL(query);
			query = "CREATE  TABLE IF NOT EXISTS Activite_FichierLIRC ( idFichierLIRC INT NOT NULL , idTypeActivite INT NOT NULL ,  PRIMARY KEY (idFichierLIRC, idTypeActivite) ,  CONSTRAINT FK4    FOREIGN KEY (idFichierLIRC )    REFERENCES FichierLIRC (idFichierLIRC )    ON DELETE CASCADE    ON UPDATE CASCADE,  CONSTRAINT FK6    FOREIGN KEY (idTypeActivite )    REFERENCES TypeActivite (idTypeActivite )    ON DELETE CASCADE    ON UPDATE CASCADE)";
			db.execSQL(query);
			query = "CREATE  TABLE IF NOT EXISTS TypeActivite (  idTypeActivite INTEGER PRIMARY KEY AUTOINCREMENT ,  nomActivite INT NOT NULL )";
			db.execSQL(query);
			query = "CREATE  TABLE IF NOT EXISTS Alarm ( date VARCHAR(45) NOT NULL, heure VARCHAR(45) NOT NULL, dureeCycle VARCHAR(45) NOT NULL )";
			db.execSQL(query);

			// insertDefaultActivitiesTypes
			ArrayList<String> defaultActivities = Const.getDefaultActivitiesTypes();
			for (int i = 0; i < defaultActivities.size(); i++) {
				query = "INSERT INTO typeactivite (idTypeActivite, nomActivite) VALUES ('" + i + "', '" + defaultActivities.get(i)
						+ "')";
				db.execSQL(query);
			}

		} // end method onCreate

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		} // end method onUpgrade
	} // end class DatabaseOpenHelper

	public void deleteAlarmFromProfil(String alarmString) {
		String[] separated = alarmString.split(";");
		open();
		database.delete("Alarm", "date='" + separated[0] + "' AND heure='" +separated[1] + "' AND dureeCycle='" +separated[2] +"'", null);
		close();
	}

	public void addAlarmForProfil(String date, String heure, String dureeCycle) {
		open();
		ContentValues newAlarm = new ContentValues();
		newAlarm.put("date", date);
		newAlarm.put("heure", heure);
		newAlarm.put("dureeCycle", dureeCycle);
		database.insert("Alarm", null, newAlarm);
		close();
	}

	public ArrayList<String> getAlarms() {
		open();
		Cursor mCursor = database.query("Alarm", null, null, null, null, null, null);
		int dateIndex = mCursor.getColumnIndex("date");
		int heureIndex = mCursor.getColumnIndex("heure");
		int dureeCycleIndex = mCursor.getColumnIndex("dureeCycle");
		ArrayList<String> mArrayList = new ArrayList<String>();
		for (mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
			mArrayList.add(mCursor.getString(dateIndex)+";"+mCursor.getString(heureIndex)+";"+mCursor.getString(dureeCycleIndex));
		}
		close();
		return mArrayList;
	}
}

/**************************************************************************
 * (C) Copyright 1992-2012 by Deitel & Associates, Inc. and * Pearson Education, Inc. All Rights Reserved. * * DISCLAIMER: The
 * authors and publisher of this book have used their * best efforts in preparing the book. These efforts include the * development,
 * research, and testing of the theories and programs * to determine their effectiveness. The authors and publisher make * no
 * warranty of any kind, expressed or implied, with regard to these * programs or to the documentation contained in these books. The
 * authors * and publisher shall not be liable in any event for incidental or * consequential damages in connection with, or arising
 * out of, the * furnishing, performance, or use of these programs. *
 **************************************************************************/
