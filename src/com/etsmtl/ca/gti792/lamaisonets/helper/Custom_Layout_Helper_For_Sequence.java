package com.etsmtl.ca.gti792.lamaisonets.helper;

import com.etsmtl.ca.gti792.lamaisonets.activity.LaMaisonETS_Control_Tele_Activity;
import com.etsmtl.ca.gti792.lamaisonets.activity.sequence.LaMaisonETS_Control_Lumiere_Sequence_Activity;
import com.etsmtl.ca.gti792.lamaisonets.activity.sequence.LaMaisonETS_Control_Tele_Sequence_Activity;

/**
 * Classe Helper permettant de r�cup�rer les layout de s�quence correspondant au type d'activit� demand�.
 * 
 * @author Fran�ois
 * 
 */
public class Custom_Layout_Helper_For_Sequence{

	private static final String CONTROL_TELE = "control_television_layout_sequence";
	private static final String CONTROL_LUMIERE = "control_lumiere_layout_sequence";
	private static final String CONTROL_ROBOT = "control_robot_layout_sequence";
	private static final String CONTROL_AUTRE = "control_autre_layout_sequence";

	public static String getLayoutPath(String type) {
		if (type.equals("Controle televiseur"))
			return CONTROL_TELE;
		else if (type.equals("Controle lumiere"))
			return CONTROL_LUMIERE;
		else if (type.equals("Controle robot"))
			return null;
		else if (type.equals("Controle autre"))
			return null;
		else {
			return type;
		}
	}

	public static Class<?> getClassFromLayoutName(String layout) {
		if (layout.equals(CONTROL_TELE))
			return LaMaisonETS_Control_Tele_Sequence_Activity.class;
		else if (layout.equals(CONTROL_LUMIERE))
			return LaMaisonETS_Control_Lumiere_Sequence_Activity.class;
		else if (layout.equals(CONTROL_ROBOT))
			return null;
		else if (layout.equals(CONTROL_AUTRE))
			return null;
		else {
			return LaMaisonETS_Control_Tele_Activity.class;
		}
	}
}
