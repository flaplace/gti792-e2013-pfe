package com.etsmtl.ca.gti792.lamaisonets.helper;

import com.etsmtl.ca.gti792.lamaisonets.activity.LaMaisonETS_Control_Lumiere_Activity;
import com.etsmtl.ca.gti792.lamaisonets.activity.LaMaisonETS_Control_Robot_Activity;
import com.etsmtl.ca.gti792.lamaisonets.activity.LaMaisonETS_Control_Tele_Activity;

/**
 * Classe Helper permettant de r�cup�rer les layout de contr�le correspondant au type d'activit� demand�.
 * 
 * @author Fran�ois
 * 
 */
public class Custom_Layout_Helper_For_Control_Activity {

	private static final String CONTROL_TELE = "control_television_layout";
	private static final String CONTROL_LUMIERE = "control_lumiere_layout";
	private static final String CONTROL_ROBOT = "control_robot_layout";
	private static final String CONTROL_AUTRE = "control_autre_layout";

	public static String getLayoutPath(String type) {
		if (type.equals("Controle televiseur"))
			return CONTROL_TELE;
		else if (type.equals("Controle lumiere"))
			return CONTROL_LUMIERE;
		else if (type.equals("Controle robot"))
			return CONTROL_ROBOT;
		else if (type.equals("Controle autre"))
			return CONTROL_ROBOT;
		else {
			return type;
		}
	}

	public static Class<?> getClassFromLayoutName(String layout) {
		if (layout.equals(CONTROL_TELE))
			return LaMaisonETS_Control_Tele_Activity.class;
		else if (layout.equals(CONTROL_LUMIERE))
			return LaMaisonETS_Control_Lumiere_Activity.class;
		else if (layout.equals(CONTROL_ROBOT))
			return LaMaisonETS_Control_Robot_Activity.class;
		else if (layout.equals(CONTROL_AUTRE))
			return LaMaisonETS_Control_Tele_Activity.class;
		else {
			return LaMaisonETS_Control_Tele_Activity.class;
		}
	}
}
