package com.etsmtl.ca.gti792.lamaisonets.dbmodel;


// Generated 22-Jul-2013 3:00:28 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

/**
 * Profil generated by hbm2java
 */
public class Profil implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9095978175626551335L;
	private Integer idProfil;
	private String nomProfil;
	private Preference preference;
	private Set<Activite> activites = new HashSet<Activite>(0);

	public Profil() {
	}

	public Profil(String nomProfil) {
		this.nomProfil = nomProfil;
	}

	public Profil(String nomProfil, Preference preference, Set<Activite> activites) {
		this.nomProfil = nomProfil;
		this.preference = preference;
		this.activites = activites;
	}

	public Integer getIdProfil() {
		return this.idProfil;
	}

	public void setIdProfil(Integer idProfil) {
		this.idProfil = idProfil;
	}

	public String getNomProfil() {
		return this.nomProfil;
	}

	public void setNomProfil(String nomProfil) {
		this.nomProfil = nomProfil;
	}

	public Preference getPreference() {
		return this.preference;
	}

	public void setPreference(Preference preference) {
		this.preference = preference;
	}

	public Set<Activite> getActivites() {
		return this.activites;
	}

	public void setActivites(Set<Activite> activites) {
		this.activites = activites;
	}

}
