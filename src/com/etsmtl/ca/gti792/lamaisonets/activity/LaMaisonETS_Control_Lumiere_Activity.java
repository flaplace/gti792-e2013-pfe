package com.etsmtl.ca.gti792.lamaisonets.activity;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.etsmtl.ca.gti792.lamaisonets.lirc.Lirc;

import android.app.Activity;
import android.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;

import android.util.Log;

import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activit� Android. Cette classe est l'activit� permettant � l'utilisateur de contr�ler tr�s sp�cifiquement une activit� de type
 * Lumi�re.<BR />
 * <BR />
 * Cette activit� est rattach� au layout: control_lumiere_layout.xml <BR />
 * 
 * @author Fran�ois
 * 
 */
public class LaMaisonETS_Control_Lumiere_Activity extends Activity implements LaMaisonETS_Control_Interface {

	private byte buffer[];
	private SharedPreferences mPrefs;
	private AudioManager audio;
	private AudioTrack ir;
	private int bufSize = AudioTrack.getMinBufferSize(48000, AudioFormat.CHANNEL_CONFIGURATION_STEREO,
			AudioFormat.ENCODING_PCM_8BIT);
	private final static String LIRCD_CONF_FILE = Environment.getExternalStorageDirectory().getPath() + "/tmp/t.conf";
	private String mycmd;
	private Handler mHandler = new Handler();
	
	TextView tv;
	static Lirc lirc = new Lirc();

	ArrayAdapter<String> commandList;

	int minBufSize;
	private Vibrator myVib;
	public String device;
	
	private Runnable dim_up_runnable = new Runnable() {
		public void run() {

			ir.release();

			String gcmd = "Raise";
			myVib.vibrate(50);
//			Log.i("SendIR","YAA!");
			try {
				sendSignal(device, gcmd);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
			mHandler.postAtTime(this, SystemClock.uptimeMillis() + 200);
		}
	};
	
	private Runnable dim_down_runnable = new Runnable() {
		public void run() {

			ir.release();

			String gcmd = "Lower";
			myVib.vibrate(50);
//			Log.i("SendIR","YAA!");
			try {
				sendSignal(device, gcmd);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
			mHandler.postAtTime(this, SystemClock.uptimeMillis() + 200);
		}
	};
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		firstRunPreferences();

		int now = getVolume();
		lirc = new Lirc();
		setContentView(R.layout.control_lumiere_layout);
		Button button_ouvrir_lumieres = (Button) findViewById(R.id.button_ouvrir_lumieres);
		Button button_fermer_lumieres = (Button) findViewById(R.id.button_fermer_lumieres);
		ImageButton button_dim_up_lumieres = (ImageButton) findViewById(R.id.button_dim_up_lumieres);
		ImageButton button_dim_down_lumieres = (ImageButton) findViewById(R.id.button_dim_down_lumieres);
		Button button_mode_scene = (Button) findViewById(R.id.button_mode_scene);
		final Spinner spinCommand = (Spinner) findViewById(R.id.SpinnerCommande);
		commandList = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
		commandList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		myVib = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);

		// Prepare audio buffer
		minBufSize = AudioTrack.getMinBufferSize(48000, AudioFormat.CHANNEL_CONFIGURATION_STEREO, AudioFormat.ENCODING_PCM_8BIT);

		audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		if (audio.isBluetoothA2dpOn()) {
			audio.setBluetoothA2dpOn(true);
			audio.setStreamVolume(AudioManager.STREAM_MUSIC, now, 0);
			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		} else {
			audio.setStreamVolume(AudioManager.STREAM_MUSIC, now, 0);
			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}

		String value = getIntent().getExtras().getString("lircFile");
		parse(value);
		device = lirc.getDeviceList()[0];

		spinCommand.setPrompt("Select a command");
		spinCommand.setAdapter(commandList);

		spinCommand.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

				mycmd = spinCommand.getSelectedItem().toString();
				if (ir != null) {
					try {
						sendSignal(device, mycmd);
					} catch (IllegalStateException e) {
						e.printStackTrace();
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}

		});
		button_ouvrir_lumieres.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View view, MotionEvent motionevent) {
				int action = motionevent.getAction();
				if (action == MotionEvent.ACTION_DOWN) {
					myVib.vibrate(50);
					String gcmd = "On";
					try {
						sendSignal(device, gcmd);
					} catch (IllegalStateException e) {
						e.printStackTrace();
					}
				} else if (action == MotionEvent.ACTION_UP) {
					try {
						Thread.sleep(150);
						if (ir != null) {
							ir.flush();
							ir.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				return false;
			}
		});

		button_fermer_lumieres.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View view, MotionEvent motionevent) {
				int action = motionevent.getAction();
				if (action == MotionEvent.ACTION_DOWN) {
					myVib.vibrate(50);
					String gcmd = "Off";
					try {
						sendSignal(device, gcmd);
					} catch (IllegalStateException e) {
						e.printStackTrace();
					}
				} else if (action == MotionEvent.ACTION_UP) {
					try {
						Thread.sleep(150);
						if (ir != null) {
							ir.flush();
							ir.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				return false;
			}
		});

		button_dim_up_lumieres.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View view, MotionEvent motionevent) {
				int action = motionevent.getAction();
				if (action == MotionEvent.ACTION_DOWN) {
					myVib.vibrate(50);
					String gcmd = "Raise";
					try {
						sendSignal(device, gcmd);
					} catch (IllegalStateException e) {
						e.printStackTrace();
					}
					mHandler.postAtTime(dim_up_runnable, SystemClock.uptimeMillis() + 200);
				} else if (action == MotionEvent.ACTION_UP) {
					try {
						Thread.sleep(150);
						if (ir != null) {
							ir.flush();
							ir.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					mHandler.removeCallbacks(dim_up_runnable);
				}
				return false;
			}
		});

		button_dim_down_lumieres.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View view, MotionEvent motionevent) {
				int action = motionevent.getAction();
				if (action == MotionEvent.ACTION_DOWN) {
					myVib.vibrate(50);
					String gcmd = "Lower";
					try {
						sendSignal(device, gcmd);
					} catch (IllegalStateException e) {
						e.printStackTrace();
					}
					mHandler.postAtTime(dim_down_runnable, SystemClock.uptimeMillis() + 200);
				} else if (action == MotionEvent.ACTION_UP) {
					try {
						Thread.sleep(150);
						if (ir != null) {
							ir.flush();
							ir.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					mHandler.removeCallbacks(dim_down_runnable);
				}
				return false;
			}
		});

		button_mode_scene.setOnTouchListener(new OnTouchListener() {
			public boolean onTouch(View view, MotionEvent motionevent) {
				int action = motionevent.getAction();
				if (action == MotionEvent.ACTION_DOWN) {
					myVib.vibrate(50);
					String gcmd = "Scene";
					try {
						sendSignal(device, gcmd);
					} catch (IllegalStateException e) {
						e.printStackTrace();
					}
				} else if (action == MotionEvent.ACTION_UP) {
					try {
						Thread.sleep(150);
						if (ir != null) {
							ir.flush();
							ir.release();
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				return false;
			}
		});
	}

	public String about() {
		AlertDialog.Builder about = new AlertDialog.Builder(this);
		about.setTitle(R.string.app_name).setIcon(R.drawable.ic_launcher).setMessage(R.string.info).setCancelable(true)
				.setNegativeButton("OK!", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});

		AlertDialog welcomeMenu = about.create();

		welcomeMenu.show();
		Button button = welcomeMenu.getButton(AlertDialog.BUTTON_NEGATIVE);
		button.setCompoundDrawablesWithIntrinsicBounds(this.getResources().getDrawable(R.drawable.check_ok), null, null, null);
		button.setTextSize(12);
		return null;

	}

	public boolean parse(String config_file) {

		java.io.File file = new java.io.File(config_file);

		if (!file.exists()) {
			if (config_file != LIRCD_CONF_FILE)
				Toast.makeText(getApplicationContext(), "The Selected file doesn't exist", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(getApplicationContext(), "Configuartion file missing, please update the db", Toast.LENGTH_SHORT)
						.show();
			return false;
		}

		if (lirc.parse(config_file) == 0) {
			Toast.makeText(getApplicationContext(), "Couldn't parse the selected file", Toast.LENGTH_SHORT).show();
			return false;
		}

		// Save the file since it has been parsed successfully
		if (config_file != LIRCD_CONF_FILE) {
			try {
				FileInputStream in = new FileInputStream(config_file);
				FileOutputStream out = new FileOutputStream(LIRCD_CONF_FILE);
				byte[] buf = new byte[1024];
				int i = 0;
				while ((i = in.read(buf)) != -1) {
					out.write(buf, 0, i);
				}
				in.close();
				out.close();
			} catch (Exception e) {
				tv.append("Probleme saving configuration file: " + e.getMessage());
			}
		}

		updateDeviceList();

		return true;
	}

	public void updateDeviceList() {
		String[] str = lirc.getDeviceList();

		if (str == null) {
			Toast.makeText(getApplicationContext(), "Invalid, empty or missing config file", Toast.LENGTH_SHORT).show();
			return;
		}
		device = str[0];
		Log.e("ANDROLIRC", "Device list successfuly updated. Number of devices: " + String.valueOf(str.length));
		updateCommandList(str[0]);

	}

	public void updateCommandList(String device) {
		String[] str = lirc.getCommandList(device);

		if (str == null) {
			Toast.makeText(getApplicationContext(), "No command found for the selected device", Toast.LENGTH_SHORT).show();
			return;
		}

		commandList.clear();
		for (int i = 0; i < str.length; i++)
			commandList.add(str[i]);

		Log.e("ANDROLIRC", "Command list successfuly updated. Number of detected commands: " + String.valueOf(str.length));
	}

	public void sendSignal(String device, String cmd) {

		buffer = lirc.getIrBuffer(device, cmd, minBufSize + 1024);

		if (buffer == null) {
			Toast.makeText(getApplicationContext(), "Empty Buffer!", Toast.LENGTH_SHORT).show();
			return;
		}
		ir = new AudioTrack(AudioManager.STREAM_MUSIC, 48000, AudioFormat.CHANNEL_CONFIGURATION_STEREO,
				AudioFormat.ENCODING_PCM_8BIT, bufSize, AudioTrack.MODE_STATIC);

		if (bufSize < buffer.length)
			bufSize = buffer.length;

		ir.write(buffer, 0, buffer.length);
		ir.setStereoVolume(1, 1);
		ir.play();

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Pr�f�rences").setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(0, 1, 0, "Envoie").setIcon(android.R.drawable.arrow_up_float);
		menu.add(0, 2, 0, "Aide").setIcon(android.R.drawable.ic_menu_help);
		return true;
	}

	/**
	 * 
	 * get if this is the first run
	 * 
	 * @return returns true, if this is the first run
	 */
	public boolean getFirstRun() {
		return mPrefs.getBoolean("firstRun", true);
	}

	public int getVolume() {
		return mPrefs.getInt("volume", 50);
	}

	public void setVolume(int volume) {
		SharedPreferences.Editor edit = mPrefs.edit();
		edit.putInt("volume", volume);
		edit.commit();
	}

	public void saveCurrentRemote(String remote) {
		SharedPreferences.Editor edit = mPrefs.edit();
		edit.putString("CurrentRemote", remote);
		edit.commit();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
			int volume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
			setVolume(volume);
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
			int volume_dn = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
			setVolume(volume_dn);
			return true;
		case KeyEvent.KEYCODE_BACK:
			finish();
			// System.exit(0);
		default:
			super.onKeyDown(keyCode, event);
			return false;
		}
	}

	/**
	 * 
	 * setting up preferences storage
	 */
	public void firstRunPreferences() {
		Context mContext = this.getApplicationContext();
		mPrefs = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
	}

	@Override
	public void onStop() {
		super.onStop();
		if (audio.isBluetoothA2dpOn()) {

			audio.setBluetoothA2dpOn(true);
			// audio.setStreamVolume(AudioManager.STREAM_MUSIC,now, 0);

			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}
		lirc = new Lirc();

		if (ir != null) {
			ir.flush();
			ir.release();
			ir = null;
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (ir != null) {
			ir.flush();
			ir.release();
			ir = null;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			Intent gererPreferenceIntent = new Intent(LaMaisonETS_Control_Lumiere_Activity.this,
					LaMaisonETS_GererPreferences_Activity.class);
			startActivity(gererPreferenceIntent);
			break;
		case 1:
			try {
				sendSignal(device, mycmd);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
			break;
		case 2:
			about();
			break;
		}
		return false;
	}
}