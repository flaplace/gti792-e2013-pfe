package com.etsmtl.ca.gti792.lamaisonets.activity;

import com.etsmtl.ca.gti792.lamaisonets.helper.Custom_Layout_Helper_For_Control_Activity;
import com.etsmtl.ca.gti792.lamaisonets.helper.Custom_Layout_Helper_For_Sequence;
import com.etsmtl.ca.gti792.lamaisonets.helper.DatabaseConnectorHelper;

import android.app.Activity;
import android.app.AlertDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;

import android.view.KeyEvent;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;

import android.widget.TextView;

/**
 * Classe d'accueil de l'application LaMaisonETS. C'est une Activit� Android. La premi�re fois que cette classe est ouverte, un
 * message de bienvenue doit �tre affich�. Cette activit� permet un point d'entr�e avec l'application en affichant toutes les
 * activit�s en lien avec le profil s�lectionn�. � l'ouverture de l'application, le profil par d�faut s�lectionn� est le premier �
 * �tre r�cup�rer par la classe DatabaseConnectorHelper. <BR />
 * <BR />
 * Cette classe affiche toutes les activit�s du profil � l'aide d'une vue accord�on, ou bien un messge indiquand qu'aucune activit�
 * n'est associ�e au profil. Le menu Android de cette Activit� permet �galement de rediriger l'utilisateur vers les autres activit�s
 * de l'application, tel que: gestion des pr�f�rences, aide, analyser une fichier LIRC, ajouter une nouvelle activit� ou bien
 * choisir un profil.<BR />
 * <BR />
 * Inspiration pour le menu accord�on: http://jatin4rise.wordpress.com/2011/07/
 * 
 * @author Fran�ois
 * 
 */
public class LaMaisonETS_Accueil_Activity extends Activity {

	private static final int ADD_NEW_ACTIVITY_REQUEST = 0;
	private static final int CHOISIR_PROFIL_REQUEST = 1;

	byte buffer[];
	protected String com;
	protected String dev;
	protected SharedPreferences mPrefs;
	private AudioManager audio;
	private LinearLayout accordion;
	private TextView textProfilName;
	private DatabaseConnectorHelper dbHelper;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = DatabaseConnectorHelper.getInstance(getApplicationContext());
		firstRunPreferences();
		int now = getVolume();
		setContentView(R.layout.accueil_main);
		accordion = (LinearLayout) findViewById(R.id.activity_list);
		textProfilName = (TextView) findViewById(R.id.nom_profil);
		textProfilName.setText(dbHelper.getProfil_name());

		audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		fillActivityAccordion();

		if (audio.isBluetoothA2dpOn()) {
			audio.setBluetoothA2dpOn(true);
			audio.setStreamVolume(AudioManager.STREAM_MUSIC, now, 0);
			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		} else {
			audio.setStreamVolume(AudioManager.STREAM_MUSIC, now, 0);
			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}

		if (getFirstRun()) {
			about();
			setRunned();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		textProfilName.setText(dbHelper.getProfil_name());
		clearActivityTable();
		fillActivityAccordion();
	}

	void fillActivityAccordion() {

		Button button;
		TextView text;
		if (dbHelper.getActivity_names().size() == 0) {
			findViewById(R.id.nom_activite).setVisibility(View.VISIBLE);
			findViewById(R.id.nom_activite).setClickable(false);
			accordion.setVisibility(View.GONE);
			findViewById(R.id.liste_activite_textView).setVisibility(View.GONE);
		} else {
			findViewById(R.id.nom_activite).setVisibility(View.GONE);
			accordion.setVisibility(View.VISIBLE);
			findViewById(R.id.liste_activite_textView).setVisibility(View.VISIBLE);
		}
		for (int activityName = 0; activityName < dbHelper.getActivity_names().size(); activityName++) {

			button = new Button(this);
			button.setId(activityName);
			button.setText(dbHelper.getActivity_names().get(activityName));
			button.setTypeface(null, 1);
			button.setTextSize(15);
			button.setGravity(Gravity.LEFT);

			/**
			 * By default colour of button is black
			 */
			button.setTextColor(getResources().getColor(R.color.black));
			button.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.sq_br_down, 0);

			accordion.addView(button, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			button.setOnClickListener(new View.OnClickListener() {

				public void onClick(View buttonView) {
					Button button = (Button) buttonView;
					LinearLayout linearLayout = (LinearLayout) buttonView.getParent();

					/**
					 * It text color is black open the accordion of selected tab close the accordion of remaining tab else if text
					 * color is white close the accordion of selected tab
					 */
					if (button.getCurrentTextColor() == getResources().getColor(R.color.black)) {

						/**
						 * OPEN CHILD OF SELECTED TAB AND CLOSE REMAINING PREVIOUSLY OPENED TABS
						 */
						for (int j = 0; j < linearLayout.getChildCount(); j++) {
							if (button.getId() == linearLayout.getChildAt(j).getId()) {

								// Change color, so that we can distinguish the tab which are selected
								button.setTextColor(getResources().getColor(R.color.white));

								// Change visibility
								linearLayout.getChildAt(j).setVisibility(View.VISIBLE);
								for (int activityOption = 0; activityOption < dbHelper.getActivity_options().size(); activityOption++) {
									linearLayout.getChildAt(j + activityOption + 1).setVisibility(View.VISIBLE);
								}
								// Change icon
								button.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.sq_br_up, 0);
							}
						}
					} else {
						/**
						 * CLOSE OTHER TABS
						 */
						for (int j = 0; j < linearLayout.getChildCount(); j++) {
							if (button.getId() == linearLayout.getChildAt(j).getId()
									&& linearLayout.getChildAt(j).getVisibility() == View.VISIBLE) {

								// Change color, so that we can distinguish the tab which are selected
								button.setTextColor(getResources().getColor(R.color.black));

								// Change visibility
								for (int activityOption = 0; activityOption < dbHelper.getActivity_options().size(); activityOption++) {
									linearLayout.getChildAt(j + activityOption + 1).setVisibility(View.GONE);
								}
								// Change icon
								button.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.sq_br_down, 0);
							}
						}
					}
				}
			});

			for (int activityOption = 0; activityOption < dbHelper.getActivity_options().size(); activityOption++) {
				text = new TextView(this);
				text.setId(100 * (activityName + 1) + activityOption);
				text.setText(dbHelper.getActivity_options().get(activityOption));
				text.setTypeface(null, 1);
				text.setTextSize(15);
				text.setGravity(Gravity.LEFT);
				text.setPadding(20, 10, 10, 10);
				text.setVisibility(View.GONE);
				text.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View arg0) {
						TextView text = (TextView) arg0;
						int parentButtonId = Integer.valueOf(String.valueOf(text.getId()).substring(0, 1)) - 1; // sketch..
						if (text.getText().equals("Ouvrir activit�")) {
							try {

								String layout = Custom_Layout_Helper_For_Control_Activity.getLayoutPath(dbHelper
										.getActiviteTypeNameFromActivityName(((Button) findViewById(parentButtonId))
												.getText().toString()));
								Class<?> clazz = Custom_Layout_Helper_For_Control_Activity
										.getClassFromLayoutName(layout);
								Intent openActivityIntent = new Intent(
										LaMaisonETS_Accueil_Activity.this,
										clazz);
								openActivityIntent.putExtra(
										"lircFile",
										dbHelper.getFileNameFromActivityName(((Button) findViewById(parentButtonId))
												.getText().toString()));
								startActivity(openActivityIntent);
							} catch (Exception e) {
								toBeImplementedDialog();
							}
						} else if (text.getText().equals("Programmer activit�")) {
							try {
								String layout = Custom_Layout_Helper_For_Sequence.getLayoutPath(dbHelper
										.getActiviteTypeNameFromActivityName(((Button) findViewById(parentButtonId))
												.getText().toString()));
								Class<?> clazz = Custom_Layout_Helper_For_Sequence
										.getClassFromLayoutName(layout);
								Intent openActivityIntent = new Intent(
										LaMaisonETS_Accueil_Activity.this,
										clazz);
								openActivityIntent.putExtra(
										"lircFile",
										dbHelper.getFileNameFromActivityName(((Button) findViewById(parentButtonId))
												.getText().toString()));
								startActivity(openActivityIntent);
							} catch (Exception e) {
								toBeImplementedDialog();
							}

						} else if (text.getText().equals("Supprimer activit�")) {
							dbHelper.deleteActivityFromProfil(((Button) findViewById(parentButtonId)).getText().toString());
							// hide corresponding view...
							clearActivityTable();
							fillActivityAccordion();
						}
					}

				

				});
				accordion.addView(text, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			}
		}
	}

	private void toBeImplementedDialog() {
		AlertDialog.Builder about = new AlertDialog.Builder(this);
		about.setTitle("Erreur")
				.setIcon(android.R.drawable.ic_dialog_info)
				.setMessage(
						"Cette fonctionalit� n'a pas encore �t� impl�ment�e")
				.setCancelable(false)
				.setNegativeButton("Retour",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});

		AlertDialog alert = about.create();
		alert.show();
		Button button = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
		button.setTextSize(12);
	}
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Pr�f�rences").setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(0, 1, 0, "Aide").setIcon(android.R.drawable.ic_menu_help);
		menu.add(0, 2, 0, "Ajouter nouvelle activit�").setIcon(android.R.drawable.ic_menu_add);
		menu.add(0, 3, 0, "Analyser fichier LIRC").setIcon(android.R.drawable.ic_menu_upload);
		menu.add(0, 4, 0, "Chosir profil").setIcon(R.drawable.ic_menu_allfriends);
		return true;
	}

	/**
	 * 
	 * get if this is the first run
	 * 
	 * @return returns true, if this is the first run
	 */
	public boolean getFirstRun() {
		return mPrefs.getBoolean("firstRun", true);
	}

	public int getVolume() {
		return mPrefs.getInt("volume", 50);
	}

	public String getRemote() {
		return mPrefs.getString("CurrentRemote", "");
	}

	/**
	 * 
	 * store the first run
	 */
	public void setRunned() {
		SharedPreferences.Editor edit = mPrefs.edit();
		edit.putBoolean("firstRun", false);
		edit.commit();
	}

	public void setVolume(int volume) {
		SharedPreferences.Editor edit = mPrefs.edit();
		edit.putInt("volume", volume);
		edit.commit();
	}

	public void saveCurrentRemote(String remote) {
		SharedPreferences.Editor edit = mPrefs.edit();
		edit.putString("CurrentRemote", remote);
		edit.commit();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
			int volume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
			setVolume(volume);
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			audio.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
			int volume_dn = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
			setVolume(volume_dn);
			return true;
		case KeyEvent.KEYCODE_BACK:
			finish();
			System.exit(0);
		default:
			super.onKeyDown(keyCode, event);
			return false;
		}
	}

	/**
	 * 
	 * setting up preferences storage
	 */
	public void firstRunPreferences() {
		Context mContext = this.getApplicationContext();
		mPrefs = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (audio.isBluetoothA2dpOn()) {

			audio.setBluetoothA2dpOn(true);
			// audio.setStreamVolume(AudioManager.STREAM_MUSIC,now, 0);

			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			Intent gererPreferenceIntent = new Intent(LaMaisonETS_Accueil_Activity.this,
					LaMaisonETS_GererPreferences_Activity.class);
			startActivity(gererPreferenceIntent);
			break;
		case 1:
			about();
			break;
		case 2:
			Intent ajoutNouvActiviteIntent = new Intent(LaMaisonETS_Accueil_Activity.this, LaMaisonETS_Ajouter_Activity.class);
			startActivityForResult(ajoutNouvActiviteIntent, ADD_NEW_ACTIVITY_REQUEST);
			break;
		case 3:
			Intent ajoutFichierLIRCIntent = new Intent(LaMaisonETS_Accueil_Activity.this,
					LaMaisonETS_AjoutFichierLIRC_Activity.class);
			startActivity(ajoutFichierLIRCIntent);
			break;
		case 4:
			Intent choisirProfilIntent = new Intent(LaMaisonETS_Accueil_Activity.this, LaMaisonETS_ChoisirProfil_Activity.class);
			startActivityForResult(choisirProfilIntent, CHOISIR_PROFIL_REQUEST);
			break;
		}
		return false;
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && requestCode == ADD_NEW_ACTIVITY_REQUEST) {
			clearActivityTable();
			fillActivityAccordion();
		} else if (resultCode == RESULT_OK && requestCode == CHOISIR_PROFIL_REQUEST) {
			clearActivityTable();
			fillActivityAccordion();
		}

	}

	private void clearActivityTable() {
		accordion.removeAllViews();
	}

	public String about() {
		AlertDialog.Builder about = new AlertDialog.Builder(this);
		about.setTitle(R.string.app_name).setIcon(R.drawable.ic_launcher).setMessage(R.string.info).setCancelable(true)
				.setNegativeButton("OK!", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});

		AlertDialog welcomeMenu = about.create();

		welcomeMenu.show();
		// Make the textview clickable. Must be called after show()
		// ((TextView) welcomeMenu.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
		Button button = welcomeMenu.getButton(AlertDialog.BUTTON_NEGATIVE);
		button.setCompoundDrawablesWithIntrinsicBounds(this.getResources().getDrawable(R.drawable.check_ok), null, null, null);
		button.setTextSize(12);
		return null;

	}
}