package com.etsmtl.ca.gti792.lamaisonets.activity.sequence;

import com.etsmtl.ca.gti792.lamaisonets.activity.LaMaisonETS_Control_Interface;
import com.etsmtl.ca.gti792.lamaisonets.activity.LaMaisonETS_Control_Lumiere_Activity;
import com.etsmtl.ca.gti792.lamaisonets.activity.R;
import com.etsmtl.ca.gti792.lamaisonets.helper.DatabaseConnectorHelper;
import com.etsmtl.ca.gti792.lamaisonets.lirc.Lirc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

/**
 * Activit� Android. Cette classe est similaire � la classe {@link LaMaisonETS_Control_Lumiere_Activity} et en diff�re par le layout
 * et les fonctionalit�s. Elle permet une interface programmable permettant de configurer un r�veil pour l'activit� de contr�le de
 * lumi�res. <BR />
 * <BR />
 * Cette classe est rattach�e au layout control_lumiere_sequence_layout.xml
 * 
 * @author Fran�ois
 */
public class LaMaisonETS_Control_Tele_Sequence_Activity extends Activity implements LaMaisonETS_Control_Interface {

	private byte buffer[];
	private int bufSize = AudioTrack.getMinBufferSize(48000, AudioFormat.CHANNEL_CONFIGURATION_STEREO,
			AudioFormat.ENCODING_PCM_8BIT);
	private int minBufSize;
	private SharedPreferences mPrefs;
	private DatabaseConnectorHelper dbHelper;
	private LinearLayout accordion;
	private AudioTrack ir;
	private static Lirc lirc = new Lirc();
	private AudioManager audio;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		firstRunPreferences();
		dbHelper = DatabaseConnectorHelper.getInstance(getApplicationContext());
		setContentView(R.layout.control_television_sequence_layout);
		accordion = (LinearLayout) findViewById(R.id.alarm_list);
		int volume_int = getVolume();
		audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		if (audio.isBluetoothA2dpOn()) {
			audio.setBluetoothA2dpOn(true);
			audio.setStreamVolume(AudioManager.STREAM_MUSIC, volume_int, 0);
			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		} else {
			audio.setStreamVolume(AudioManager.STREAM_MUSIC, volume_int, 0);
			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}

		fillProfilAccordion();
		Button ok = ((Button) findViewById(R.id.button_ok));
		Button cancel = ((Button) findViewById(R.id.button_cancel));
		Button supprimerAlarmeButton = (Button) findViewById(R.id.supprimer_alarme);
		if (dbHelper.getAlarms().size() == 0) {
			supprimerAlarmeButton.setVisibility(View.GONE);
		} else {
			supprimerAlarmeButton.setVisibility(View.VISIBLE);
		}
		supprimerAlarmeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				for (int i = 0; i < accordion.getChildCount(); i++) {
					TextView text = (TextView) accordion.getChildAt(i);
					if (text.getCurrentTextColor() == getResources().getColor(R.color.red)) {
						dbHelper.deleteAlarmFromProfil(text.getText().toString());
						text.setVisibility(View.GONE);
					}
				}
			}
		});
		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				try {
					DatePicker dpresult = (DatePicker) findViewById(R.id.dpResult);
					TimePicker timePicker1 =  ((TimePicker) findViewById(R.id.timePicker1));
					
					String date = String.valueOf(dpresult.getDayOfMonth()) + "/" + String.valueOf(dpresult.getMonth()+1) + "/" + String.valueOf(dpresult.getYear());
					String heure = String.valueOf(timePicker1.getCurrentHour())+":"+ String.valueOf(timePicker1.getCurrentMinute());
					String dureeCycle = ((EditText) findViewById(R.id.editText1)).getText().toString();
					dbHelper.addAlarmForProfil(date,heure,dureeCycle);
					alarmAjouterDialog();
				} catch (Exception e) {
				}
			}

			

		});

		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}

		});
	}

	private void alarmAjouterDialog() {
		AlertDialog.Builder about = new AlertDialog.Builder(this);
		about.setTitle("Confirmation")
				.setIcon(android.R.drawable.ic_dialog_info)
				.setMessage(
						"Alarme ajout�e")
				.setCancelable(false)
				.setNegativeButton("Retour",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								finish();
							}
						});

		AlertDialog alert = about.create();
		alert.show();
		Button button = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
		button.setTextSize(12);
	}
	void fillProfilAccordion() {

		TextView text;
		if (dbHelper.getAlarms().size() == 0) {
			accordion.setVisibility(View.GONE);
		} else {
			accordion.setVisibility(View.VISIBLE);
		}
		for (int alarm = 0; alarm < dbHelper.getAlarms().size(); alarm++) {

			text = new Button(this);
			text.setId(alarm);
			text.setText(dbHelper.getAlarms().get(alarm));
			text.setTypeface(null, 1);
			text.setTextSize(15);
			text.setGravity(Gravity.LEFT);
			text.setOnClickListener(new View.OnClickListener() {

				public void onClick(View view) {
					// Set selected...
					TextView text = (TextView) view;
					if (text.getCurrentTextColor() != getResources().getColor(R.color.red)) {
						text.setTextColor(getResources().getColor(R.color.red));
					} else {
						text.setTextColor(getResources().getColor(R.color.black));
					}
				}
			});
			accordion.addView(text, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (audio.isBluetoothA2dpOn()) {

			audio.setBluetoothA2dpOn(true);
			// audio.setStreamVolume(AudioManager.STREAM_MUSIC,now, 0);

			audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		}
		lirc = new Lirc();

		if (ir != null) {
			ir.flush();
			ir.release();
			ir = null;
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (ir != null) {
			ir.flush();
			ir.release();
			ir = null;
		}
	}

	@Override
	public void sendSignal(String device, String cmd) {
		buffer = lirc.getIrBuffer(device, cmd, minBufSize + 1024);

		if (buffer == null) {
			Toast.makeText(getApplicationContext(), "Empty Buffer!", Toast.LENGTH_SHORT).show();
			return;
		}
		ir = new AudioTrack(AudioManager.STREAM_MUSIC, 48000, AudioFormat.CHANNEL_CONFIGURATION_STEREO,
				AudioFormat.ENCODING_PCM_8BIT, bufSize, AudioTrack.MODE_STATIC);

		if (bufSize < buffer.length)
			bufSize = buffer.length;

		ir.write(buffer, 0, buffer.length);
		ir.setStereoVolume(1, 1);
		ir.play();

	}

	public int getVolume() {
		return mPrefs.getInt("volume", 50);
	}

	/**
	 * 
	 * setting up preferences storage
	 */
	public void firstRunPreferences() {
		Context mContext = this.getApplicationContext();
		mPrefs = mContext.getSharedPreferences("myAppPrefs", Context.MODE_PRIVATE);
	}
}
