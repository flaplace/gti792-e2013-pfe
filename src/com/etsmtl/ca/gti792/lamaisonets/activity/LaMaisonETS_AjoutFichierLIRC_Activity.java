package com.etsmtl.ca.gti792.lamaisonets.activity;

import java.util.ArrayList;

import com.etsmtl.ca.gti792.lamaisonets.h3r3t1c.filechooser.FileChooser_Activity;
import com.etsmtl.ca.gti792.lamaisonets.helper.DatabaseConnectorHelper;
import com.etsmtl.ca.gti792.lamaisonets.lirc.Lirc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activit� Android. Cette classe permet d'ajouter un fichier LIRC � la configuration de l'application et de l'associer � un type
 * d'activit�, retenue dans une base de donn�es SQLite. Elle utilise le {@link DatabaseConnectorHelper} pour r�aliser cet
 * enregistrement. <BR />
 * <BR />
 * Actuellement, cette activit� d�marre l'activit� de s�lection de fichier {@link FileChooser_Activity} et n�c�ssite un r�sultat
 * pour proc�der � son affichage. Si l'utilisateur cancel la s�lection de fichier, l'activit�
 * {@link LaMaisonETS_AjoutFichierLIRC_Activity} se termine et retourne � l'activit� pr�c�dante.
 * 
 * @author Fran�ois
 * 
 */
public class LaMaisonETS_AjoutFichierLIRC_Activity extends Activity {

	private final static String LIRCD_CONF_FILE = Environment.getExternalStorageDirectory().getPath() + "/tmp/t.conf";
	private static final int CHOOSE_FILE_RESULT = 2;
	private Lirc lirc;
	private ArrayAdapter<String> activityListAdapter;
	private DatabaseConnectorHelper dbHelper;
	private Spinner spinnerActivityType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.ajout_fichier_lirc);
		dbHelper = DatabaseConnectorHelper.getInstance(getApplicationContext());
		lirc = new Lirc();
		Intent chooseFileIntent = new Intent(LaMaisonETS_AjoutFichierLIRC_Activity.this, FileChooser_Activity.class);
		startActivityForResult(chooseFileIntent, CHOOSE_FILE_RESULT);
		spinnerActivityType = ((Spinner) findViewById(R.id.spinner_choisir_type_activite));
		Button ok = ((Button) findViewById(R.id.button_ok));
		Button cancel = ((Button) findViewById(R.id.button_cancel));

		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != ((TextView) findViewById(R.id.nom_activite)).getText()
						&& parse(((TextView) findViewById(R.id.nom_activite)).getText().toString()))
					dbHelper.insertNewLIRCFile(((TextView) findViewById(R.id.nom_activite)).getText().toString(),
							spinnerActivityType.getSelectedItem().toString());
					Toast.makeText(getApplicationContext(), "Fichier LIRC ajout�", Toast.LENGTH_SHORT).show();
				finish();
			}

		});

		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}

		});
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && requestCode == CHOOSE_FILE_RESULT) {
			((TextView) findViewById(R.id.nom_activite)).setText(data.getStringExtra("file"));
			ArrayList<String> activityTypes = dbHelper.getActivity_types();

			// Initialize adapter for device spinner
			activityListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
			activityListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			// Initialize device spinner
			spinnerActivityType.setPrompt("Choisir type d'activit�");
			spinnerActivityType.setAdapter(activityListAdapter);

			activityListAdapter.clear();
			for (String activityType : activityTypes) {
				activityListAdapter.add(activityType);
			}
		} else {
			finish();
		}
	}

	public boolean parse(String config_file) {

		java.io.File file = new java.io.File(config_file);

		if (!file.exists()) {
			if (config_file != LIRCD_CONF_FILE)
				Toast.makeText(getApplicationContext(), "Le fichier s�lectionn� n'existe pas", Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(getApplicationContext(), "Le fichier s�lectionn� n'existe pas", Toast.LENGTH_SHORT)
						.show();
			return false;
		}

		if (lirc.parse(config_file) == 0) {
			Toast.makeText(getApplicationContext(), "Impossible d'analyser le fichier, v�rifier le format et le contenu", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	// @Override
	// protected void onStop() {
	// super.onStop();
	// lirc = new Lirc();
	// }

}
