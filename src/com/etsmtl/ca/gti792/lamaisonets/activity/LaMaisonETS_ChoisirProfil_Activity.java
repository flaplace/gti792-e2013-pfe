package com.etsmtl.ca.gti792.lamaisonets.activity;

import java.util.ArrayList;

import com.etsmtl.ca.gti792.lamaisonets.helper.DatabaseConnectorHelper;
import com.etsmtl.ca.gti792.lamaisonets.helper.DialogHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

/**
 * Activit� Android. Cette classe est l'activit� permettant � l'utilisateur de changer de profil. Un profil est un regroupement
 * d'activit�. Le concept de profil permet � l'utilisatuer de contr�ler plus efficacement des appareils qui se trouve � proximit�.
 * Par exemple, des profils r�v�lateur serait Maison, Chalet, Travail, etc.<BR />
 * <BR />
 * �galement, le menu de cette activit� permet de supprimer en totalit� la configuration de l'application. Cette op�ration efface
 * toutes les tables de la bases de donn�es et r�clame la confirmation de l'utilisateur avant d'�tre ex�cut�e.<BR />
 * <BR />
 * Il est �galement possible � l'utilisateur d'acc�der � ses pr�f�rences � l'int�rieur de cette activit�.
 * 
 * @author Fran�ois
 * 
 */
public class LaMaisonETS_ChoisirProfil_Activity extends Activity {

	private DatabaseConnectorHelper dbHelper;
	private ArrayAdapter<String> profilListAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = DatabaseConnectorHelper.getInstance(getApplicationContext());
		setContentView(R.layout.choisir_profil);
		Button ok = ((Button) findViewById(R.id.button_ok));
		Button cancel = ((Button) findViewById(R.id.button_cancel));
		Button button_gerer_profil = ((Button) findViewById(R.id.button_gerer_profil));
		Button button_creer_profil = ((Button) findViewById(R.id.button_creer_profil));

		final Spinner spinnerProfil = ((Spinner) findViewById(R.id.spinner_choisir_format_horloge));

		// Initialize adapter for device spinner
		profilListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
		profilListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinnerProfil.setPrompt("Choisir profil");
		spinnerProfil.setAdapter(profilListAdapter);
		refreshProfilListSpinner();

		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != spinnerProfil.getSelectedItem()) {
					dbHelper.selectProfil(spinnerProfil.getSelectedItem().toString());
					setResult(RESULT_OK);
				}
				finish();
			}

		});

		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}

		});

		button_gerer_profil.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dbHelper.selectProfil(spinnerProfil.getSelectedItem().toString());
				Intent gererProfilIntent = new Intent(LaMaisonETS_ChoisirProfil_Activity.this,
						LaMaisonETS_GererProfil_Activity.class);
				startActivity(gererProfilIntent);
			}

		});

		button_creer_profil.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final EditText input = new EditText(getApplicationContext());
				new AlertDialog.Builder(LaMaisonETS_ChoisirProfil_Activity.this).setTitle("Nouveau profil")
						.setMessage("Veuillez entrer le nom du nouveau profil").setView(input)
						.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								dbHelper.insertNewProfile(input.getText().toString());
								refreshProfilListSpinner();
							}
						}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
								// Do nothing.
							}
						}).show();
			}

		});
	}

	@Override
	public void onResume() {
		super.onResume();
		refreshProfilListSpinner();
	}

	private void refreshProfilListSpinner() {
		ArrayList<String> profilList = dbHelper.getProfil_list();
		profilListAdapter.clear();

		for (String profil : profilList) {
			profilListAdapter.add(profil);
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Pr�f�rences").setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(0, 1, 0, "R�initialiser app.").setIcon(android.R.drawable.ic_menu_close_clear_cancel);
		menu.getItem(1).setOnMenuItemClickListener(myclick);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			Intent gererPreferenceIntent = new Intent(LaMaisonETS_ChoisirProfil_Activity.this,
					LaMaisonETS_GererPreferences_Activity.class);
			startActivity(gererPreferenceIntent);
			break;
		case 1:
			break;
		}
		return false;
	}

	public final MenuItem.OnMenuItemClickListener myclick = new OnMenuItemClickListener() {

		@Override
		public boolean onMenuItemClick(MenuItem item) {
			doclick();
			return false;
		}
	};

	public void doclick() {
		DialogHandler appdialog = new DialogHandler();
		appdialog
				.Confirm(
						this,
						"R�initialiser l'application?",
						"Toutes les donn�es seront perdues apr�s suppression de votre configuration et il ne sera pas possible de les restaurer!",
						"Cancel", "OK", aproc(), bproc());
	}

	public Runnable aproc() {
		return new Runnable() {
			public void run() {
				dbHelper.flushDB();
				finish();
			}
		};
	}

	public Runnable bproc() {
		return new Runnable() {
			public void run() {
			}
		};
	}

}
