package com.etsmtl.ca.gti792.lamaisonets.activity;

import java.util.ArrayList;

import com.etsmtl.ca.gti792.lamaisonets.helper.Const;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

/**
 * Activit� Android. Cette classe permet � l'utilisateur de g�rer ses pr�f�rences. En ce moment, les deux pr�f�rences qui sont
 * personnifiable sont: la langue et le format de l'horloge. <BR />
 * <BR />
 * Cette classe n'est pas encore impl�ment�e.
 * 
 * @author Fran�ois
 */
public class LaMaisonETS_GererPreferences_Activity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gerer_preferences);

		Button ok = ((Button) findViewById(R.id.button_ok));
		Button cancel = ((Button) findViewById(R.id.button_cancel));

		final Spinner spinner_choisir_langue = ((Spinner) findViewById(R.id.spinner_choisir_langue));
		final Spinner spinner_choisir_format_horloge = ((Spinner) findViewById(R.id.spinner_choisir_format_horloge));

		ArrayList<String> lircFilesList = Const.getLanguageList();
		ArrayList<String> profilList = Const.getClockFormat();

		ArrayAdapter<String> choisir_langue_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
		ArrayAdapter<String> choisir_format_horloge_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);

		choisir_langue_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		choisir_format_horloge_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// Initialize device spinner
		spinner_choisir_langue.setPrompt("Langue");
		spinner_choisir_langue.setAdapter(choisir_langue_adapter);

		spinner_choisir_format_horloge.setPrompt("Format d'affichage de l'heure");
		spinner_choisir_format_horloge.setAdapter(choisir_format_horloge_adapter);

		choisir_langue_adapter.clear();
		choisir_format_horloge_adapter.clear();

		for (String lircFile : lircFilesList) {
			choisir_langue_adapter.add(lircFile);
		}

		for (String profil : profilList) {
			choisir_format_horloge_adapter.add(profil);
		}

		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// FIXME
				// Sauvegarder les pr�f�rences
				finish();
			}

		});

		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}

		});
	}
}
