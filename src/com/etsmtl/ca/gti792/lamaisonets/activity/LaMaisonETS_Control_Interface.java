package com.etsmtl.ca.gti792.lamaisonets.activity;

/**
 * Interface pour les activit�s de contr�le direct et de contr�le par s�quence.<BR />
 * <BR />
 * Chacune des activit� de contr�le doit impl�menter imp�rativement cette classe.
 * 
 * @author Fran�ois
 * 
 */
public interface LaMaisonETS_Control_Interface {
	public abstract void sendSignal(String device, String cmd);

	public void onStop();

	public void onDestroy();
}
