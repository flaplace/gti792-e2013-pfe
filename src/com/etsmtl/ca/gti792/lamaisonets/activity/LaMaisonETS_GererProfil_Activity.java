package com.etsmtl.ca.gti792.lamaisonets.activity;

import com.etsmtl.ca.gti792.lamaisonets.helper.DatabaseConnectorHelper;
import com.etsmtl.ca.gti792.lamaisonets.helper.DialogHandler;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

/**
 * Antivit� Android. Cette classe permet de g�rer le profil de l'utilisateur. Si le profil actuel poss�de une ou plusieurs
 * activit�s, celles-ci sont ajout�e dans une liste o� l'utilisateur peut s�lectionner les activit�s qu'ils d�sirent supprimer. Une
 * fois s�lectionn�e, le text du nom de l'activit� devient rouge. Les activit�s s�lectionn�es peuvent donc �tre toutes supprimer en
 * m�me temps � l'aide d'un bouton situ� en bas de l'activit�. <BR />
 * <BR />
 * �galement, il est possible pour l'utilisateur d'acc�der au menu de cette activit� pour supprimer compl�tement le profil
 * s�lectionn�. Cette op�ration demande la confirmation de l'utilisateur et permet de supprimer compl�tement le profil de la base de
 * donn�es. <BR />
 * <BR />
 * Il est �galement possible � l'utilisateur d'acc�der � ses pr�f�rences � l'int�rieur de cette activit�.
 * 
 * @author Fran�ois
 * 
 */
public class LaMaisonETS_GererProfil_Activity extends Activity {

	private DatabaseConnectorHelper dbHelper;
	private LinearLayout accordion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = DatabaseConnectorHelper.getInstance(getApplicationContext());
		setContentView(R.layout.gerer_profil);

		accordion = (LinearLayout) findViewById(R.id.activity_list);
		TextView textProfilName = (TextView) findViewById(R.id.nom_profil);
		textProfilName.setText(dbHelper.getProfil_name());
		fillProfilAccordion();
		Button supprimerActiviteButton = (Button) findViewById(R.id.supprimer_activite);
		Button ok = ((Button) findViewById(R.id.button_ok));
		Button cancel = ((Button) findViewById(R.id.button_cancel));
		supprimerActiviteButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				for (int i = 0; i < accordion.getChildCount(); i++) {
					TextView text = (TextView) accordion.getChildAt(i);
					if (text.getCurrentTextColor() == getResources().getColor(R.color.red)) {
						dbHelper.deleteActivityFromProfil(text.getText().toString());
						text.setVisibility(View.GONE);
					}
				}
			}
		});
		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}

		});

		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}

		});
	}

	void fillProfilAccordion() {

		TextView text;
		if (dbHelper.getActivity_names().size() == 0) {
			accordion.setVisibility(View.GONE);
		} else {
			accordion.setVisibility(View.VISIBLE);
		}
		for (int activityName = 0; activityName < dbHelper.getActivity_names().size(); activityName++) {

			text = new Button(this);
			text.setId(activityName);
			text.setText(dbHelper.getActivity_names().get(activityName));
			text.setTypeface(null, 1);
			text.setTextSize(15);
			text.setGravity(Gravity.LEFT);
			text.setOnClickListener(new View.OnClickListener() {

				public void onClick(View view) {
					// Set selected...
					TextView text = (TextView) view;
					if (text.getCurrentTextColor() != getResources().getColor(R.color.red)) {
						text.setTextColor(getResources().getColor(R.color.red));
					} else {
						text.setTextColor(getResources().getColor(R.color.black));
					}
				}
			});
			accordion.addView(text, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Pr�f�rences").setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(0, 1, 0, "Supprimer profil").setIcon(android.R.drawable.ic_menu_close_clear_cancel);
		menu.getItem(1).setOnMenuItemClickListener(myclick);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			// TODO
			break;
		case 1:
			break;
		}
		return false;
	}

	public final MenuItem.OnMenuItemClickListener myclick = new OnMenuItemClickListener() {

		@Override
		public boolean onMenuItemClick(MenuItem item) {
			doclick();
			return false;
		}
	};

	public void doclick() {
		DialogHandler appdialog = new DialogHandler();
		appdialog
				.Confirm(
						this,
						"Supprimer le profil?",
						"Toutes les donn�es du profil seront perdues apr�s suppression de ce profil et il ne sera pas possible de les restaurer!",
						"Cancel", "OK", aproc(), bproc());
	}

	public Runnable aproc() {
		return new Runnable() {
			public void run() {
				dbHelper.deleteProfil();
				finish();
			}
		};
	}

	public Runnable bproc() {
		return new Runnable() {
			public void run() {
			}
		};
	}

}
