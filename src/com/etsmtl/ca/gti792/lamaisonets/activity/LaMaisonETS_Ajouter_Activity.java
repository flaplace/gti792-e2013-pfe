package com.etsmtl.ca.gti792.lamaisonets.activity;

import java.util.ArrayList;

import com.etsmtl.ca.gti792.lamaisonets.helper.DatabaseConnectorHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Classe d'ajout d'activit� au profil de l'application LaMaisonETS. C'est une Activit� Android. Cette classe permet d'ajouter une
 * nouvelle activit� au profil actuel. Elle r�clame � l'utilisateur le nom de l'activit�, le fichier LIRC correspondant
 * (pr�alablement analyser dans l'activit� {@link LaMaisonETS_AjoutFichierLIRC_Activity}, ainsi que le nom du profil auquel il faut
 * ajouter la nouvelle activit�. Par d�faut, le profil actif est s�lectionn� dans le combobox. <BR />
 * <BR />
 * Cette activit� comporte �galement un menu permettant d'acc�der aux pr�f�rences, ou bien de supprimer en totalit� la configuration
 * de l'application. Un message de confirmation reclame la confirmation de l'utilisateur pour r�initialiser l'application au
 * complet.
 * 
 * @author Fran�ois
 * 
 */
public class LaMaisonETS_Ajouter_Activity extends Activity {

	private DatabaseConnectorHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = DatabaseConnectorHelper.getInstance(getApplicationContext());
		setContentView(R.layout.ajout_nouv_activite);
		Button ok = ((Button) findViewById(R.id.button_ok));
		Button cancel = ((Button) findViewById(R.id.button_cancel));

		final Spinner spinnerLircFile = ((Spinner) findViewById(R.id.spinner_choisir_fichier_lirc));
		final Spinner spinnerProfil = ((Spinner) findViewById(R.id.spinner_choisir_profil));

		ArrayList<String> lircFilesList = dbHelper.getLirc_list();
		ArrayList<String> profilList = dbHelper.getProfil_list();

		// Initialize adapter for device spinner
		ArrayAdapter<String> lircListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);
		ArrayAdapter<String> profilListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item);

		lircListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		profilListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// Initialize device spinner
		spinnerLircFile.setPrompt("Choisir fichier LIRC");
		spinnerLircFile.setAdapter(lircListAdapter);

		spinnerProfil.setPrompt("Choisir profil");
		spinnerProfil.setAdapter(profilListAdapter);

		lircListAdapter.clear();
		profilListAdapter.clear();

		for (String lircFile : lircFilesList) {
			lircListAdapter.add(lircFile);
		}

		for (String profil : profilList) {
			profilListAdapter.add(profil);
		}

		spinnerProfil.setSelection(profilListAdapter.getPosition(dbHelper.getProfil_name()));
		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != ((TextView) findViewById(R.id.nom_activite)).getText()
						&& null != ((Spinner) findViewById(R.id.spinner_choisir_fichier_lirc)).getSelectedItem()
						&& null != ((Spinner) findViewById(R.id.spinner_choisir_profil)).getSelectedItem()) {
					dbHelper.insertNewActivity(((TextView) findViewById(R.id.nom_activite)).getText().toString(),
							((Spinner) findViewById(R.id.spinner_choisir_fichier_lirc)).getSelectedItem().toString(),
							((Spinner) findViewById(R.id.spinner_choisir_profil)).getSelectedItem().toString());
					setResult(Activity.RESULT_OK, null);
					finish();
				} else {
					showErrorDialog();
				}

			}

		});

		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}

		});
	}

	public String showErrorDialog() {
		AlertDialog.Builder about = new AlertDialog.Builder(this);
		about.setTitle("Erreur").setIcon(android.R.drawable.ic_dialog_alert)
				.setMessage("Impossible d'enregistrer la nouvelle activit�, v�rifier les informations SVP").setCancelable(true)
				.setNegativeButton("Retour", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});

		AlertDialog alert = about.create();

		alert.show();
		Button button = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
		button.setTextSize(12);
		return null;

	}
}
